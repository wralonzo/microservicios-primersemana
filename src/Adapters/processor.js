const Services = require('../Services');

//mensaje de error
const { InternalError } = require('../settings');

const { queueCreate, queueDelete, queueUpdate, queueFindOne, queueView } = require('./index');

queueView.process(async function (job, done){

    try{

        const { id } = job.data;

        console.log(id);

        let { statuscode, data, message } = await Services.View({  });
        
        done(null, { statuscode, data, message });

    }catch(error){
        console.log({ step: 'Adapter queueView', error: error.toString() });
        
        done(null, { statuscode: 500, message: InternalError } );
    }
    
});

queueCreate.process(async function (job, done){

    try{

        const { age, color, name } = job.data;

        let { statuscode, data, message } = await Services.Create({ age, color, name });
        
        done(null, { statuscode, data, message });

    }catch(error){
        console.log({ step: 'Adapter queueCreate', error: error.toString() });
        
        done(null, { statuscode: 500, message: InternalError } );
    }
    
});

queueDelete.process(async function (job, done){

    try{

        const { id } = job.data;

        let { statuscode, data, message } = await Services.Delete({ id });
        
        done(null, { statuscode, data, message });

    }catch(error){
        console.log({ step: 'Adapter queueDelete', error: error.toString() });
        
        done(null, { statuscode: 500, message: InternalError } );
    }
    
});

queueFindOne.process(async function (job, done){

    try{

        const { name } = job.data;

        let { statuscode, data, message } = await Services.View({ name });
        
        done(null, { statuscode, data, message });

    }catch(error){
        console.log({ step: 'Adapter queueFindOne', error: error.toString() });
        
        done(null, { statuscode: 500, message: InternalError } );
    }
    
});

queueUpdate.process(async function (job, done){

    try{

        const { age, color, name, id } = job.data;

        let { statuscode, data, message } = await Services.View({ age, color, name, id });
        
        done(null, { statuscode, data, message });

    }catch(error){
        console.log({ step: 'Adapter queueUpdate', error: error.toString() });
        
        done(null, { statuscode: 500, message: InternalError } );
    }
    
});


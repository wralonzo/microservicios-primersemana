//Libreria para usar las variasbles de entorno
const dotenv = require('dotenv');

//importar el orm de Sequelizer
const { Sequelizer } = require('sequelize');

dotenv.config();

//Definidas en el archivo .env
const redis = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT
}

const InternalError = 'No podemos procesar la solicitud en este momento';

//Variables que se obtienen del virtual env
const sequelize = new Sequelizer({
    host: process.env.POSTGRES_HOST,
    port: process.env.POSTGRES_PORT,
    database: process.env.POSTGRES_DB,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    dialect: 'postgres'
});

module.exports = { redis, InternalError, sequelize };
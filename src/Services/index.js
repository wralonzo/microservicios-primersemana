const Controller = require('../Controllers');

async function Create ({ age, color, name }) {
    try {

        let { statuscode, data, menssage } = await Controller.Create({ age, color, name });
    
         return { statuscode, data, menssage  }

    } catch (error) {

        console.log({step: 'Servicio Create', error: error.toString()});
        
        return { statuscode: 500, message: error.toString()};
    }
}
async function Delete ({ id }) {
    try {

        let { statuscode, data, menssage } = await Controller.Delete({ where: { id } });
    
         return { statuscode, data, menssage  }

    } catch (error) {
        console.log({step: 'Servicio Delete', error: error.toString()});

        return { statuscode: 500, message: error.toString()};
    }
}
async function Update ({ age, color, name, id }) {
    try {

        let { statuscode, data, menssage } = await Controller.Update({ age, color, name, id });
    
         return { statuscode, data, menssage  }

    } catch (error) {
        console.log({step: 'Servicio Update', error: error.toString()});

        return { statuscode: 500, message: error.toString()};
    }
}
async function FindOne ({ name }) {
    try {

        let { statuscode, data, menssage } = await Controller.FindOne({ name });
    
         return { statuscode, data, menssage  }

    } catch (error) {
        console.log({step: 'Servicio Find one', error: error.toString()});

        return { statuscode: 500, message: error.toString()};
    }
}
async function View ({  }) {
    try {

        let { statuscode, data, menssage } = await Controller.View({  });
    
         return { statuscode, data, menssage  }

    } catch (error) {
        console.log({step: 'Servicio View', error: error.toString()});

        return { statuscode: 500, message: error.toString()};
    }
}
module.exports = { Create, Delete, Update, FindOne, View }
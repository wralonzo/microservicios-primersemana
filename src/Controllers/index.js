const { Model } = require('../Models');

async function Create({ name, age, color }){
    try{

        let instance = await Model.create(
            { name, age, color }, 
            { fields: ['name', 'age', 'colors']}
        );
        return { statuscode: 200, data: instance.toJSON() };


    }catch(error){
        console.log({
            step: 'Controlador Create',
            error: error.toString()
        })
        return {
            statuscode:400,
            menssage: error.toString()
        }
    }
}
async function Delete({ where = {} }){
    try{

        let instance = await Model.destroy( { where }  );

        return { statuscode: 200, data: 'Borrado OK' };

    }catch(error){
        console.log({
            step: 'Controlador Delete',
            error: error.toString()
        })
        return {
            statuscode:400,
            menssage: error.toString()
        }
    }
}
async function Update({ name, age, color, id }){
    try{

        let instance = await Model.update(
            { name, age, color }, 
            { where: { id }}
        );
        return { statuscode: 200, data: instance[1][0].toJSON() };

    }catch(error){
        console.log({
            step: 'Controlador Update',
            error: error.toString()
        })
        return {
            statuscode:400,
            menssage: error.toString()
        }
    }
}
async function FindOne({ where = {} }){
    try{

        let instance = await Model.findOne({ where });

        return { statuscode: 200, data: instance.toJSON() };

    }catch(error){
        console.log({
            step: 'Controlador FindOne',
            error: error.toString()
        })
        return {
            statuscode:400,
            menssage: error.toString()
        }
    }
}
async function View({ where = {} }){
    try{

        let instances = await Model.findAll({ where });

        return { statuscode: 200, data: instances };

    }catch(error){
        console.log({
            step: 'Controlador View',
            error: error.toString()
        })
        return {
            statuscode:400,
            menssage: error.toString()
        }
    }
}
module.exports = { Create, Delete, Update, FindOne, View}
